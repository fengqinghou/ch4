public class Student
{
    public string Name { get; set; }
    public int Age { get; set; }
    public string Gender { get; set; }
}

public class Class
{
    private List<Student> students = new List<Student>();

    public void AddStudent(Student student)
    {
        students.Add(student);
    }

    public Student this[int index]
    {
        get { return students[index]; }
        set { students[index] = value; }
    }
}