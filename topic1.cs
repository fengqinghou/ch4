﻿using System;
public class MyStack<T>
{
    private T[] items;
    private int top;

    public MyStack(int size)
    {
        items = new T[size];
        top = -1;
    }

    public void InitStack()
    {
        top = -1;
    }

    public void ClearStack()
    {
        top = -1;
        Array.Clear(items, 0, items.Length);
    }

    public bool Push(T item)
    {
        if (top == items.Length - 1)
        {
            return false;
        }

        top++;
        items[top] = item;
        return true;
    }

    public T Pop()
    {
        if (top == -1)
        {
            return default(T);
        }

        T item = items[top];
        top--;
        return item;
    }
}