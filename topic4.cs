using System;
namespace topic
{
    public delegate bool GradePrint(Student s);

    public class Student
    {
        public GradePrint GP { set; get; }
    }

    public class GradeReport
    {
        public static bool GradeReportOrderByTerm(Student s)
        {
            // 在这里编写打印成绩单的逻辑
            return true;
        }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student();
            student.GP = GradeReport.GradeReportOrderByTerm;

            // 调用委托打印成绩单
            bool result = student.GP(student);

            Console.WriteLine("成绩单打印结果：" + result);
        }
    }
}