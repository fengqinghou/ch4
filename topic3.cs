using System;

// 气象站类
class WeatherStation
{
    public DateTime LastUpdateTime { get; set; }
    public double Temperature { get; set; }
    public double Humidity { get; set; }

    public event EventHandler<WeatherChangedEventArgs> WeatherChanged;

    public void UpdateWeather(double temperature, double humidity)
    {
        Temperature = temperature;
        Humidity = humidity;
        LastUpdateTime = DateTime.Now;

        OnWeatherChanged(new WeatherChangedEventArgs(Temperature, Humidity));
    }

    protected virtual void OnWeatherChanged(WeatherChangedEventArgs e)
    {
        WeatherChanged?.Invoke(this, e);
    }
}

// 气象数据改变事件参数类
class WeatherChangedEventArgs : EventArgs
{
    public double Temperature { get; }
    public double Humidity { get; }

    public WeatherChangedEventArgs(double temperature, double humidity)
    {
        Temperature = temperature;
        Humidity = humidity;
    }
}

// 屏幕显示类
class ScreenDisplay
{
    public void ShowWeather(double temperature, double humidity)
    {
        Console.WriteLine($"Temperature: {temperature}°C, Humidity: {humidity}%");
    }
}

// 数据库保存类
class DatabaseSaver
{
    public void SaveWeather(DateTime updateTime, double temperature, double humidity)
    {
        // 将气象数据保存到数据库中
        Console.WriteLine($"Weather data saved to database: UpdateTime={updateTime}, Temperature={temperature}, Humidity={humidity}");
    }
}

// 测试
class Program
{
    static void Main(string[] args)
    {
        WeatherStation station = new WeatherStation();
        ScreenDisplay screen = new ScreenDisplay();
        DatabaseSaver saver = new DatabaseSaver();

        // 订阅气象数据改变事件
        station.WeatherChanged += (sender, e) =>
        {
            screen.ShowWeather(e.Temperature, e.Humidity);
            saver.SaveWeather(station.LastUpdateTime, e.Temperature, e.Humidity);
        };

        // 模拟气象数据改变
        station.UpdateWeather(25.0, 60.0);
        station.UpdateWeather(26.0, 65.0);
    }
}